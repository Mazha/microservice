#!/bin/bash

echo Removing existing containers
docker stop $(docker ps -aq) && docker rm $(docker ps -aq)
