import time

import sys

from .conman_etcd import ConManEtcd
from .db_logic import Database


db = Database()


class Program:
    def __init__(self,
                 key,
                 filename,
                 host='127.0.0.1',
                 port=2379,
                 username=None,
                 password=None):
        self.conman = ConManEtcd(host=host,
                                 port=int(port),
                                 user=username,
                                 password=password,
                                 on_change=self.on_configuration_change,
                                 timeout=0)
        self.filename = filename
        open(self.filename, 'w+')
        self.key = key
        self.last_change = None
        self.run()

    def on_configuration_change(self, key, action, value):
        # Sometimes the same change is reported multiple times. Ignore repeats
        if self.last_change == (key, action, value):
            return

        self.last_change = (key, action, value)

        line = f'key: {key}, action: {action}, value: {value}\n'

        with open(self.filename, 'a') as f:
            f.write(line)

        if action == 'set':
            data = {
                "key": key,
                "action": action,
                "value": value
            }
            db.insert_one(data)
        self.conman.refresh(self.key)

    def run(self):
        self.conman.refresh(self.key)
        self.conman.watch(self.key)
        time.sleep(1)


if __name__ == '__main__':
    Program(*sys.argv[1:])
