from pymongo import MongoClient
import json
from bson import ObjectId


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


class Database:
    def __init__(self, host='127.0.0.1', port='27017'):
        self.client = MongoClient(f'mongodb://{host}:{port}/')
        self.db = self.client['rest-database']
        self.db.messages.insert_one({"title": "test", "body": "very impressive"})

    def get_all_messages(self):
        return JSONEncoder().encode([message for message in self.db.messages.find({})])

    def insert_one(self, data: dict):
        self.db.messages.insert_one(data)
        print(f'Inserted: \n {data}')
