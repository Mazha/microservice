#!/bin/bash

echo Enter cluster size.
read cluster_size
echo Creating a cluster with size ${cluster_size}

./down_cluster.sh
cd ./client/ansible/

ansible-playbook create_cluster.yml --diff -e "{'size': ${cluster_size}}"

cd ~/microservice_scripts/ && ./up_etcd.sh && ./up_app.sh && ./up_nginx.sh

